package io.github.catboardbeta.argparse;

/**
 * An option (e.g. -v, --no-preserve-root, /D, etc)
 * <br><br>
 * Used in the {@link ArgParse} class to specify the 
 * options which should be parsed from the command 
 * line arguments.
 */
public class Option {

    private final String[] identifiers;
    private final boolean  shouldHaveParam;
    private final Class<?> paramType;


    /**
     * Create a new {@link Option}, assuming that the parameter's type is a String (if it has one).
     * @param identifiers An array of {@link String}s of all identifiers for this option<br><br>
     * @param shouldHaveParam Whether or not it should have a parameter listed directory after the option,
     *                        e.g. the &#60;commit message> in git commit -m &#60;commit message>
     */
    public Option(String[] identifiers, boolean shouldHaveParam) {
        this.identifiers = identifiers;
        this.shouldHaveParam = shouldHaveParam;
        this.paramType = String.class;
    }

    /**
     * Create a new {@link Option}, assuming that the parameter's type is a String (if it has one).
     * @param identifiers An array of {@link String}s of all identifiers for this option<br><br>
     * @param shouldHaveParam Whether or not it should have a parameter listed directory after the option,
     *                        e.g. the &#60;commit message> in git commit -m &#60;commit message>
     * @param paramType The type (as a {@link Class}, not a {@link String}). Get the Class Object from
     *                  <code>{@link Object}.class</code> field.
     */
    public Option(String[] identifiers, boolean shouldHaveParam, Class<?> paramType) {
        this.identifiers = identifiers;
        this.shouldHaveParam = shouldHaveParam;
        this.paramType = paramType;
    }

    /**
     * Get an array of all the identifiers for the parent Option
     * @return Array of all identifiers
     */
    public String[] getIdentifiers() {
        return identifiers;
    }

    /**
     * Get an array of all the identifiers for the parameterized Option
     * @param option Option to get from
     * @return Array of all identifiers
     */
    public static String[] getIdentifiers(Option option) {
        return option.getIdentifiers();
    }

    /**
     * Get whether or not the parent Option requires a parameter
     * @return Whether or not it requires a parameter
     */
    public boolean shouldHaveParam() {
        return shouldHaveParam;
    }

    /**
     * Get whether or not the parameterized Option
     * requires a parameter
     * @param option The Option to get from
     * @return Whether or not it requires a parameter
     */
    public static boolean shouldHaveParam(Option option) {
        return option.shouldHaveParam();
    }

    /**
     * Get the type for the parameter of the parent Option
     * @return The parameter's type
     */
    public Class<?> getParamType() {
        return paramType;
    }

    /**
     * Get the type for the parameter of the parameterized Option
     * @param option The Option to get from
     * @return The parameter's type
     */
    public static Class<?> getParamType(Option option) {
        return option.getParamType();
    }
}
