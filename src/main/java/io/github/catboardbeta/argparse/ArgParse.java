package io.github.catboardbeta.argparse;

import java.util.ArrayList;
import java.lang.String;


/**
 * Argument Parser for java.
 * <br><br>
 * Initialize a new object with {@link #ArgParse(String)}.
 * Add parameters with {@link #addParameter(Parameter)}.
 * Add options with {@link #addOption(Option)}.
 * <br><br>
 * Parse when ready with {@link #parse(String[])}.
 */
public class ArgParse {

    private final String command;
    private ArrayList<Option> options;
    private ArrayList<Parameter> params;

    /**
     * Create a new {@link ArgParse} object.
     * @param command Name of the command or executable
     */
    public ArgParse(String command) {
        this.command = command;
    }

    /**
     * Get the set command name.
     * @return Name of the command or executable
     */
    public String getCommand() {
        return command;
    }

    /**
     * Get the {@link ArrayList<Option>} of all stored Options for the command.
     * @return the Options
     */
    public ArrayList<Option> getOptions() {
        return options;
    }

    /**
     * Override the entire list of options with the {@link ArrayList} provided.
     * @param options {@link ArrayList} of options to set.
     */
    public void setOptions(ArrayList<Option> options) {
        this.options = options;
    }

    /**
     * Get the entire list of parameters
     * @return the Parameters
     */
    public ArrayList<Parameter> getParams() {
        return params;
    }

    /**
     * Override the entire list of parameters with the {@link ArrayList} provided.
     * @param params {@link ArrayList} of parameters to set.
     */
    public void setParams(ArrayList<Parameter> params) {
        this.params = params;
    }

    /**
     * Add an {@link Option} to the list of options.
     * @param option The {@link Option} to add to the list.
     */
    public void addOption(Option option) {
        this.options.add(option);
    }

    /**
     * Add a {@link Parameter} to the list of parameters
     * @param parameter The {@link Parameter} to add to the list.
     */
    public void addParameter(Parameter parameter) {
        this.params.add(parameter);
    }

    /**
     * <font color="red"><b>WARNING: NOT YET IMPLEMENTED</b></font>
     * @link args The system args from public static void main(String[] args)
     */
    public void parse(String[] args) {
        // Not Yet Implemented
    }
}
