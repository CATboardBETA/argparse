package io.github.catboardbeta.argparse;

/**
 * A parameter (e.g. the file in vim [file])
 * <br><br>
 * A parameter has 3 fields, but only 1 is required:
 * the name. The others are <code>description</code>
 * and <code>required</code>
 * <br><br>
 * Default values:<br>
 * <code>String description = ""</code><br>
 * <code>boolean required = true</code>
 */
public class Parameter {

    private final String  paramString;
    private final String  description;
    private final boolean required;

    /**
     * Create a new {@link Parameter}
     * <br><br>
     * Default values:<br>
     * <code>String description = ""</code><br>
     * <code>boolean required = true</code>
     * @param paramId Name for the parameter
     */
    public Parameter(String paramId) {
        this.paramString = paramId;
        this.description = "";
        this.required = true;
    }

    /**
     * Create a new {@link Parameter}
     * <br><br>
     * Default values:<br>
     * <code>boolean required = true</code>
     * @param paramId Name for the parameter
     * @param description Description for the parameter
     */
    public Parameter(String paramId, String description) {
        this.paramString = paramId;
        this.description = description;
        this.required = true;
    }

    /**
     * Create a new {@link Parameter}
     * <br><br>
     * Default values:<br>
     * <code>String description = ""</code><br>
     * @param paramId Name for the parameter
     * @param required Should the param be required
     */
    public Parameter(String paramId, boolean required) {
        this.paramString = paramId;
        this.description = "";
        this.required = required;
    }

    /**
     * Create a new {@link Parameter}
     * <br><br>
     * Default values:<br>
     * All values set in constructor.
     * @param paramId Name for the parameter
     * @param description  Description for the parameter
     * @param required Should the param be required
     */
    public Parameter(String paramId, String description, boolean required) {
        this.paramString = paramId;
        this.description = description;
        this.required = required;
    }

    /**
     * Get the paramID/parameter name
     * @return The parameter name
     */
    public String getParamId() {
        return paramString;
    }

    /**
     * Get the paramID/parameter name of the parameterized parameter
     * @param param The parameter to get from
     * @return The parameter name
     */
    public static String getParamId(Parameter param) {
        return param.getParamId();
    }

    /**
     * Get the description of the parameter as a {@link String}.
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Get the description of the parameterized parameter
     * @param param The param to get from
     * @return The description
     */
    public static String getDescription(Parameter param) {
        return param.getDescription();
    }

    /**
     * Get if the parameter is required
     * @return Whether or not its required
     */
    public boolean isRequired() {
        return required;
    }

    /**
     * Get whether or not the parameterized parameter is required
     * @param param The param to check
     * @return Whether or not its required
     */
    public static boolean isRequired(Parameter param) {
        return param.isRequired();
    }
}
